<!-- .slide: data-background="#00734c" -->

# Jean-Jacques Rousseau
## "Discourse on the Origin of Inequality"
## Government 101
#### Professor Scherer
![George Mason University](images/gmu-logo.jpg)


# Rousseau

## Modern Problem: Create Unity. Solution: Social Contract.

1. Antiquity could (more or less) assume a coherent "demos" and traditions of "rule."

2. Modernity defines itself as a break with tradition. . .

3. Hobbes, Locke, Rousseau, Publius all propose Contracts (or Constitutions) to solve the problem.

4. Ideas about "Natural rights," "Natural laws," and "Human nature"
inform their solutions.



# Property is central for. . .

1. LOCKE.
 * He says it derives from labour and must be "preserved." Recall: "POLITICAL POWER, then, I take to be a RIGHT of making laws with penalties of death, and consequently all less penalties, for the regulating and preserving of property, and of employing the force of the community, in the execution of such laws, and in the defence of the common-wealth from foreign injury; and all this only for the public good." (Locke's 2nd Treatise, Chapter One) 

2. CRONON.
 * He says it takes many forms and is only secured by sovereign power.

3. ROUSSEAU.
 * He has yet another story about it. . .


## More on property

1. LOCKE:
 * Property comes from labor,
 * working the land industriously,
 * and only later through formal, legal title.

2. CRONON:
 * Property is is "triadic," it only exists by convention within a sovereign polity.

3. ROUSSEAU:
 * . . . ?



# More on property in Rousseau

"The first man, who after enclosing a piece of ground, took it into his head
to say, this is mine, and found people simple enough to believe him, was the
real founder of civil society. How many crimes, how many wars, how many
murders, how many misfortunes and horrors, would that man have saved the human
species, who pulling up the stakes or filling up the ditches should have cried
to his fellows: Beware of listening to this impostor; you are lost, if you
forget that the fruits of the earth belong equally to us all, and the earth
itself to nobody!" (Rousseau, 2nd Discourse)


# Our tasks for today

1. Rousseau's argument about "Human Nature"

2. The problem of "property" in "nature" (Locke, Cronon, Rousseau)

3. Forms of property and forms of life


## Rousseau's four tasks in the Second Discourse:

1. Sketch human nature.

2. Critique inequality.

3. Relate economic inequality to political contract.

4. Question the modern form of life. . .


## Our plan for tackling Rousseau:

1. Nature/Culture

2. Rousseau's story about nature (part one of 2nd discourse).

3. Rousseau's story about inequality and culture

4. Rousseau's critique of the social contract


## The First Sentence of Rousseau's Preface to the 2nd Discourse

"OF all human sciences the most useful and most imperfect appears to me to be that of mankind: and I will venture to say, the single inscription on the Temple of Delphi contained a precept more difficult and more important than is to be found in all the huge volumes that moralists have ever written."


# "Know Yourself" (Delphic inscription)

1. Apollo's Temple: one's place as a mortal before gods.

2. Plato/Socrates: the limitations of the human as such ("human wisdom").

3. Arendt: the stories about one's actions.

4. Rousseau: human beings as they are in nature, before society changes them.


## Long quotation to that effect (review for homework)

"OF all human sciences the most useful and most imperfect appears to me to be that of mankind: and I will venture to say, the single inscription on the Temple of Delphi contained a precept more difficult and more important than is to be found in all the huge volumes that moralists have ever written. . . how shall all man hope to see himself as nature made him, across all the changes which the succession of place and time must have produced in his original constitution? How can he distinguish what is fundamental in his nature from the changes and additions which his circumstances and the advances he has made have introduced to modify his primitive condition? Like the statue of Glaucus, which was so disfigured by time, seas and tempests, that it looked more like a wild beast than a god, the human soul, altered in society by a thousand causes perpetually recurring, by the acquisition of a multitude of truths and errors, by the changes happening to the constitution of the body, and by the continual jarring of the passions, has, so to speak, changed in appearance, so as to be hardly recognisable. . . "


# Nobody has gotten back to nature. . .

The philosophers, who have inquired into the foundations of society, have all felt the necessity of going back to a state of nature; but not one of them has got there. Some of them have not hesitated to ascribe to man, in such a state, the idea of just and unjust, without troubling themselves to show that he must be possessed of such an idea, or that it could be of any use to him. Others have spoken of the natural right of every man to keep what belongs to him, without explaining what they meant by belongs. Others again, beginning by giving the strong authority over the weak, proceeded directly to the birth of government, without regard to the time that must have elapsed before the meaning of the words authority and government could have existed among men.  Every one of them, in short, constantly dwelling on wants, avidity, oppression, desires and pride, has transferred to the state of nature ideas which were acquired in society; so that, in speaking of the savage, they described the social man. (page 102)


## Do not confound "Savage" and "Civil" Man (review at home)

We should beware, therefore, of confounding the savage man with the men we have daily before our eyes. Nature treats all the animals left to her care with a predilection that seems to show how jealous she is of that right. The horse, the cat, the bull, and even the ass are generally of greater stature, and always more robust, and have more vigour, strength and courage, when they run wild in the forests than when bred in the stall. By becoming domesticated, they lose half these advantages; and it seems as if all our care to feed and treat them well serves only to deprave them. It is thus with man also: as he becomes sociable and a slave, he grows weak, timid and servile; his effeminate way of life totally enervates his strength and courage. To this it may be added that there is still a greater difference between savage and civilised man, than between wild and tame beasts: for men and brutes having been treated alike by nature, the several conveniences in which men indulge themselves still more than they do their beasts, are so many additional causes of their deeper degeneracy. (page 111)


	
# QUESTION Proposed by the Academy of Dijon

## What is the origin of inequality among men; and is it authorized by
natural law?


# Questions for us:

1. Where does inequality come from?

2. Is it justified in our society?


## If we look at human society with a calm and disinterested eye. . .

. . . it seems, at first, to show us only the violence of the powerful and
the oppression of the weak. The mind is shocked at the cruelty of the
one, or is induced to lament the blindness of the other; and as nothing
is less permanent in life than those external relations, which are more
frequently produced by accident than wisdom, and which are called
weakness or power, riches or poverty, all human institutions seem at
first glance to be founded merely on banks of shifting sand. 

---Rousseau's Preface, last paragraph--Remember David Hume?


# Inequality is not Natural

After having proved that inequality is barely perceptible in the state of nature, and that its influence there is almost null, it remains for me to show its origin and progress in the successive developments of the human mind. After having shown that perfectibility, social virtues, and the other faculties that natural man had received in potentiality could never develop by themselves, that in order to develop they needed the chance combination of several foreign causes which might never have arisen and without which he would have remained eternally in his primitive condition, it remains for me to consider and bring together the different accidents that were able to perfect human reason while deteriorating the species, make a being evil while making him sociable, and from such a distant origin finally bring man and the world to the point where we see them. (p. 140, conclusion to the first part)


# Natural Man

Four basic, natural components:

1. Self-preservation.

2. Pity. 

3. Freedom.

4. Capable of "Perfection"

Two basic things that don't exist in our nature:

1. Sociability.

2. Love of wisdom.


# How do we become cultured?


# How do we become cultured?

1. Co-location.

2. Language.

3. Reason (developed through comparison and competition)

4. Metallurgy

5. Agriculture

6. Above all, perhaps, "dependence."

It's a complicated story involving many factors. And Rousseau is ambivalent:
Culture brings bad things and good things too.


## The Golden Age is a "primitive" age

The more we reflect on this state, the more convinced we shall be, that
it was. . . the best for man, and that nothing could have drawn him out
of it but some fatal accident" (119).

"as long as they continued to consider feathers and shells as sufficient
ornaments, and to paint their bodies different colors, to improve or
ornament their bows and arrows, to fashion with sharp-edged stones
some little fishing boats, or clumsy instruments of music; in a word, as
long as they undertook such works only as a single person could finish,
and stuck to such arts as did not require the joint endeavors of several
hands, they lived free, healthy, honest and happy, as much as their
nature would admit, and continued to enjoy with each other all the
pleasures of an independent intercourse;" (120)


## Dependence (Rousseau's sentence continues)

"but from the moment one man began to stand in need of an- other's
assistance; from the moment it appeared an advantage for one man to
possess enough provisions for two, equality vanished; property was
introduced; labor became necessary; and boundless forests became
smiling fields, which had to be watered with human sweat, and in
which slavery and misery were soon seen to sprout out and grow with
the harvests."


## What's wrong with the Social Contract?

#### The Social Contract (version 1):

"Let us unite," said he, "to secure the weak from oppression, restrain
the ambitious, and secure to every man the possession of what belongs
to him: Let us form rules of justice and of peace, to which all may be
obliged to conform, which shall give no preference to anyone, but may
in some sort make amends for the caprice of fortune, by submitting
alike the powerful and the weak to the observance of mutual duties. In a
word, instead of turning our forces against ourselves, let us collect them
into a sovereign power, which may govern us by wise laws, may protect
and defend all the members of the association, repel common enemies,
and maintain a perpetual concord and harmony among us." (page 125)


## Whose concept of property is the most useful/accurate?

1. Who gets closer to the truth of property:
 * Locke
 * Cronon
 * Rousseau
 * Why?


# Is Rousseau right about the modern form of life?

"Savage man and civilized man differ so much at the bottom of their
hearts and in their inclinations, that what constitutes the supreme
happiness of the one would reduce the other to despair. The first sighs
for nothing but repose and liberty. . . Civilized man, on the other hand,
is always in motion, perpetually sweating and toiling, and racking his
brains to find out occupations still more laborious: he continues a
drudge to his last minute; nay, he courts death to be able to live, or
renounces life to acquire immortality. He pays court to men in power
whom he hates, and to rich men whom he despises; he sticks at nothing
to have the honor of serving them; he boasts proudly of his baseness
and their protection; and proud of his slavery, he speaks with disdain of
those who have not the honor of sharing it. . . How many cruel deaths
would not this indolent savage prefer to such a horrid life, which very
often is not even sweetened by the pleasure of doing good?" 


# Discussion questions

1. On Pages 118-9 Rousseau describes "nascent society" or the condition of "savages" in his day as the "golden age" of man. What is life like for savage people in this golden age? Why does Rousseau think it is the best? Why would people ever leave this condition?  

2. On pages 124-5 Rousseau says that the social contract is a very bad bargain. What's wrong with it?

3. The paragraph beginning "By thus discovering. . . " pages 137-8 recapitulates many of Rousseau's most important points in the 2nd Discourse. What is he saying here (in particular about living outside of oneself"? Does it apply to y/our lives?

4. Whose account of property is the most accurate / insightful?  Locke's, Rousseau's, Cronon's?



# THE END
