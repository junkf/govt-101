# These are the slides for Matt Scherer's Govt 101 course at George Mason University.

To make slides, write them in markdown, and save them in the markdown folder. One blank space for a vertical slide break, two blank spaces for a horizontal one.

Then use the template.html template in the root folder. Look at an existing slide if you forget what to do. The procedure is to include the markdown text within the html slide (reveal.js does the magic).


