12rousseau-social-contract
Matthew Scherer

May 25, 2016

Matthew Scherer

12rousseau-social-contract

May 25, 2016

1 / 1

GOVT 101:
Rousseau: On the Social Contract:
The General Will and The Civil State

Govt 101

Rousseau

2 / 1

II, 6 on the need for a good Legislator. . .
How would a blind multitude, which often knows not what it wishes
because it rarely knows what is good for it, execute by itself an
enterprise so great, so difcult, as a system of legislation? By
themselves, the people always desire what is good, but do not always
discern it. The general will is always right, but the judgment which
guides it is not always enlightened. The general will must be made to
see objects as they are, sometimes as they ought to appear; it must be
shown the good path that it is seeking, and guarded from the seduction
of private interests; it must be made to observe closely times and
places, and to balance the attraction of immediate and palpable
advantages against the danger of remote and concealed evils.
Individuals see the good which they reject; the public desire the good
which they do not see. All alike have need of guides.

Govt 101

Rousseau

3 / 1

Introductory note. . . "men being taken as they are"

"I MEAN to inquire if, in the civil order, there can be any sure and
legitimate rule of administration, men being taken as they are and laws
as they might be. In this inquiry I shall endeavour always to unite what
right sanctions with what is prescribed by interest, in order that justice
and utility may in no case be divided."
"As I was born a citizen of a free State, and a member of the Sovereign,
I feel that, however feeble the influence my voice can have on public
affairs, the right of voting on them makes it my duty to study them:
and I am happy, when I reflect upon governments, to find my inquiries
always furnish me with new reasons for loving that of my own country."

Govt 101

Rousseau

4 / 1

book 1 introduction: man is born free everywhere in
chains.

“MAN is born free; and everywhere he is in chains. One thinks himself
the master of others, and still remains a greater slave than they. How
did this change come about? I do not know. What can make it
legitimate? That question I think I can answer.”
“But the social order is a sacred right which is the basis of all other
rights. Nevertheless, this right does not come from nature, and must
therefore be founded on conventions. Before coming to that, I have to
prove what I have just asserted

Govt 101

Rousseau

5 / 1

What does Rousseau mean?

Recall Plato’s allegory of the cave. What does Rousseau mean here
when he says that we are in chains? What does he think our chains
are? Do you agree?

Govt 101

Rousseau

6 / 1

recall 2nd discourse, men as they are. . .
It now became the interest of men to appear what they really were not.
To be and to seem became two totally different things; and from this
distinction sprang insolent pomp and cheating trickery, with all the
numerous vices that go in their train. On the other hand, free and
independent as men were before, they were now, in consequence of a
multiplicity of new wants, brought into subjection, as it were, to all
nature, and particularly to one another; and each became in some
degree a slave even in becoming the master of other men: if rich, they
stood in need of the services of others; if poor, of their assistance; and
even a middle condition did not enable them to do without one
another. Man must now, therefore, have been perpetually employed in
getting others to interest themselves in his lot, and in making them,
apparently at least, if not really, find their advantage in promoting his
own. (continues over)

Govt 101

Rousseau

7 / 1

Continued
Thus he must have been sly and artful in his behaviour to some, and
imperious and cruel to others; being under a kind of necessity to ill-use
all the persons of whom he stood in need, when he could not frighten
them into compliance, and did not judge it his interest to be useful to
them. Insatiable ambition, the thirst of raising their respective
fortunes, not so much from real want as from the desire to surpass
others, inspired all men with a vile propensity to injure one another,
and with a secret jealousy, which is the more dangerous, as it puts on
the mask of benevolence, to carry its point with greater security. In a
word, there arose rivalry and competition on the one hand, and
conflicting interests on the other, together with a secret desire on both
of profiting at the expense of others. All these evils were the first effects
of property, and the inseparable attendants of growing inequality.

Govt 101

Rousseau

8 / 1

The General Will and The Civil State

Brief Intros.
Book I 6. The Social Compact 7. The Sovereign 8. The Civil State
Book II 3. Whether the General Will is Fallible 4. The Limits of
the Sovereign Power 7. The Legislator

Govt 101

Rousseau

9 / 1

find the form of association. . .

"Find a form of association that defends and protects the person and
goods of each associate with all the common force, and by means of
which each one, uniting with all, nevertheless obeys only himself and
remains as free as before.” (I, 6; p. 53) This is the fundamental problem
which is solved by the social contract. . .

Govt 101

Rousseau

10 / 1

recall 2nd Discourse, the original social contract.
Destitute of valid reasons to justify and sufficient strength to defend
himself, able to crush individuals with ease, but easily crushed himself
by a troop of bandits, one against all, and incapable, on account of
mutual jealousy, of joining with his equals against numerous enemies
united by the common hope of plunder, the rich man, thus urged by
necessity, conceived at length the profoundest plan that ever entered
the mind of man: this was to employ in his favour the forces of those
who attacked him, to make allies of his adversaries, to inspire them
with different maxims, and to give them other institutions as favourable
to himself as the law of nature was unfavourable.
With this view, after having represented to his neighbours the horror of
a situation which armed every man against the rest, and made their
possessions as burdensome to them as their wants, and in which no
safety could be expected either in riches or in poverty, he readily
devised plausible arguments to make them close with his design.
"Let us join," said he, "to guard the weak from oppression. . .
Govt 101

Rousseau

11 / 1

once more. . .

If then we discard from the social compact what is not of its essence,
we shall find that it reduces itself to the following terms:
"Each of us puts his person and all his power in common under the
supreme direction of the general will; and in a body we receive each
member as an indivisible part of the whole.”

Govt 101

Rousseau

12 / 1

last footnote, social contract book one,

“5. Under bad governments, this equality is only apparent and illusory:
it serves only to-keep the pauper in his poverty and the rich man in the
position he has usurped. In fact, laws are always of use to those who
possess and harmful to those who have nothing: from which it follows
that the social state is advantageous to men only when all have
something and none too much.”

Govt 101

Rousseau

13 / 1

Book I, chapter 6, state, sovereign,subject, citizen
At once, in place of the individual personality of each contracting party,
this act of association creates a moral and collective body, composed of
as many members as the assembly contains votes, and receiving from
this act its unity, its common identity, its life and its will. This public
person, so formed by the union of all other persons formerly took the
name of city,4 and now takes that of Republic or body politic; it is
called by its members State when passive. Sovereign when active, and
Power when compared with others like itself. Those who are associated
in it take collectively the name of people, and severally are called
citizens, as sharing in the sovereign power, and subjects, as being under
the laws of the State. But these terms are often confused and taken one
for another: it is enough to know how to distinguish them when they
are being used with precision.

Govt 101

Rousseau

14 / 1

Book 1, chapter 7, private and public interest (page 166)

Indeed, every individual can, as a man, have a particular will contrary
to, or divergent from, the general will which he has as a citizen; his
private interest may appear to him quite different from the common
interest; his absolute and naturally independent existence may make
him envisage what he owes to the common cause as a gratuitous
contribution, the loss of which would be less harmful to others than the
payment of it would be onerous to him; and, viewing the moral person
that constitutes the State as an abstract being because it is not a man,
he would be willing to enjoy the rights of a citizen without being willing
to fulfill the duties of a subject. The perpetua- tion of such injustice
would bring about the ruin of the body politic.

Govt 101

Rousseau

15 / 1

Book 1, chapter 7, forced to be free (page 166).

"Therefore, in order for the social compact not to be an ineffectual
formula, it tacitly includes the following engagement, which alone can
give force to the others: that whoever refuses to obey the general will
shall be constrained to do so by the entire body; which means only that
he will be forced to be free. For this is the condition that, by giving
each citizen to the homeland, guarantees him against all personal
dependence; a condition that creates the ingenuity and functioning of
the political machine, and alone gives legitimacy to civil engagements
which without it would be absurd, tyrannical, and subject to the most
enormous abuses.”

Govt 101

Rousseau

16 / 1

Book 1, chapter 8, on the civil state (page )
“This passage from the state of nature to the civil state produces a
remarkable change in man, by substituting justice for instinct in his
behavior and giving his actions the morality they previously lacked.
Only then, when the voice of duty replaces physical impulse and right
replaces appetite, does man, who until that time only considered
himself, find himself forced to act upon other principles and to consult
his reason before heeding his inclinations. Although in this state he
deprives himself of several advantages given him by nature, he gains
such great ones, his faculties are exercised and developed, his ideas
broadened, his feelings ennobled, and his whole soul elevated to such a
point that if the abuses of this new condition did not often degrade him
beneath the condition he left, he ought ceaselessly to bless the happy
moment that tore him away from it forever, and that changed him from
a stupid, limited animal into an intelligent being and a man.”

Govt 101

Rousseau

17 / 1

Book II, chapter 3 what the general will is not. . .

It follows from what precedes that the general will is always right and
always tends to the public good; but it does not follow that the
deliberations of the people always have the same rectitude. Men always
desire their own good, but do not always discern it; the people are
never corrupted, though often deceived, and it is only then that they
seem to will what is evil. There is often a great deal of difference
between the will of all and the general will; the latter regards only the
common interest, while the former has regard to private interests

Govt 101

Rousseau

18 / 1

Book II, chapter 4 what the general will might be. . .

From this we must understand that what generalizes the will is less the
number of voices than the common interest that unites them; for, under
this system, each person necessarily submits to the conditions which he
imposes on others—an admirable union of interest and justice, which
gives to the deliberations of the community a spirit of equity that seems
to disappear in debates about any private affair, for want of a common
interest to unite and identify the guidelines of the judge with that of
the concerned party. (175)

Govt 101

Rousseau

19 / 1

equality

“I shall end this chapter and this book by remarking on a fact on which
the whole social system should rest: i.e., that, instead of destroying
natural inequality, the fundamental compact substitutes, for such
physical inequality as nature may have set up between men, an equality
that is moral and legitimate, and that men, who may be unequal in
strength or intelligence, become every one equal by convention and
legal right.”

Govt 101

Rousseau

20 / 1

book II, 7, the legislator

In order to discover the rules of association that are most suitable to
nations, a superior intelligence would be necessary who could see all the
passions of men without experiencing any of them; who would have no
afnity with our nature and yet know it thoroughly; whose happiness
would not depend on us, and who would nevertheless be quite willing to
interest himself in ours; and, lastly, one who, storing up for himself with
the progress of time a far-off glory in the future, could labor in one age
and enjoy in another.* Gods would be necessary to give laws to men.

Govt 101

Rousseau

21 / 1

changing human nature
He who dares undertake to give institutions to a nation ought to feel
himself capable, as it were, of changing human nature; of transforming
every individual, who in himself is a complete and independent whole,
into part of a greater whole, from which he receives in some manner his
life and his being; of altering man’s constitution in order to strengthen
it; of sub- stituting a social and moral existence for the independent
and physical existence which we have all received from nature. In a
word, it is necessary to deprive man of his native powers in order to
endow him with some which are alien to him, and of which he cannot
make use without the aid of other people. The more thoroughly those
natural powers are deadened and de- stroyed, the greater and more
durable are the acquired powers, and the more solid and perfect also
are the institutions; so that if every citizen is nothing, and can be
nothing, except in combination with all the rest, and if the force
acquired by the whole be equal or superior to the sum of the natural
forces of all the individuals, we may say that legislation is at the
highest point of perfection which it can attain.
Govt 101

Rousseau

22 / 1

cause and effect, paradox of founding. . .

In order that a newly formed nation might approve sound maxims of
politics and observe the fundamental rules of state-policy, it would be
necessary that the effect should become the cause; that the social spirit,
which should be the product of the institution, should preside over the
institution itself, and that men should be, prior to the laws, what they
ought to become by means of them.

Govt 101

Rousseau

23 / 1

Questions for discussion
1

What makes Rousseau’s theory different from the others? Is he
persuasive? Why or why not?

2

Do we live in a republic of the sort Rousseau imagines? Are we free
in the sense he suggests?

3

Should we become more like Rousseau’s republic? Why or why not?

4

What is sovereignty according to Rousseau? How does his answer
differ from Locke’s (consider Locke’s definition of “political power”)?

5

What is the “General Will” according to Rousseau? What does it
do? (Why don’t Locke and Hobbes have a concept of the general
will?)

6

What are the key differences between living in nature, living under the false social contract, and living in the republic Rousseau
imagines?
Govt 101

Rousseau

24 / 1

