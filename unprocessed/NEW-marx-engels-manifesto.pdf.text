GOVT 101: Manifesto of the Communist Party
Prof. Scherer

Govt 101

Prof. Scherer

1 / 16

Publius, Federalist No. 10

But the most common and durable source of factions has been the
various and unequal distribution of property. Those who hold and those
who are without property have ever formed distinct interests in society.
Those who are creditors, and those who are debtors, fall under a like
discrimination. A landed interest, a manufacturing interest, a
mercantile interest, a moneyed interest, with many lesser interests,
grow up of necessity in civilized nations, and divide them into diﬀerent
classes, actuated by diﬀerent sentiments and views. . .

Govt 101

Prof. Scherer

2 / 16

Conclusions on Democracy and Politics. . .
“We have seen above, that the first step in the revolution by the
working class is to raise the proletariat to the position of ruling class
to win the battle of democracy. The proletariat will use its political
supremacy to wrest, by degree, all capital from the bourgeoisie, to
centralise all instruments of production in the hands of the State,
i.e., of the proletariat organised as the ruling class; and to increase
the total productive forces as rapidly as possible." (490)
“When, in the course of development, class distinctions have disappeared, and all production has been concentrated in the hands of a
vast association of the whole nation, the public power will lose its
political character. Political power, properly so called, is merely the
organised power of one class for oppressing another.” (491)
“In place of the old bourgeois society, with its classes and class antagonisms, we shall have an association, in which the free development
of each is the condition for the free development of all.” (491)
Govt 101

Prof. Scherer

3 / 16

Two Parts to the Story

I. Bourgeoisie and Proletariat.
“The history of all hitherto existing society is the history of class
struggles” (473).
II. Proletariat and Communists.
“The Theory of the Communists may be summed up in the single
sentence: Abolition of private property” (484).

Govt 101

Prof. Scherer

4 / 16

Another way of putting this, two concepts:

I. Class antagonism
compare to Madison on faction
II. Private property
compare to Locke, Rousseau, Madison, Cronon, etc.

Govt 101

Prof. Scherer

5 / 16

Recall: Intellectual Background

Commodities are things made by people for exchange
from C—M—C to M—C—M
The accumulation of Capital. . .
M’ = M + dM, where dM is value added by labor power*

*"The essential condition for the existence, and for the sway of the
bourgeois class, is the formation and augmentation of capital; the
condition for capital is wage-labour" (483).

Govt 101

Prof. Scherer

6 / 16

Consider: Historical/Political Background

Political Revolutions of 1848. . .
Industrial Revolution. . .
“A Spectre is haunting Europe–the spectre of Communism.”
Manifesto: published expression of understandings and intentions.
Marx and Engels are working to empower a faction / party.
Political Theory == Representation of Reality

Govt 101

Prof. Scherer

7 / 16

Against these backgrounds, Marx tells a story. . .

And this is supposed to be a diﬀerent story:
“The selfish misconception that induces you to transform into eternal
laws of nature and of reason, the social forms springing from your
present mode of production and form of property – historical relations
that rise and disappear in the progress of production – this
misconception you share with every ruling class that has preceded you.
What you see clearly in the case of ancient property, what you admit in
the case of feudal property, you are of course forbidden to admit in the
case of your own bourgeois form of property.”

Govt 101

Prof. Scherer

8 / 16

Dramatis Personae

Key characters are:
the world market
Modern Industry
Capital
Private property
the Bourgeoisie
the Proletariat
the Communists

Govt 101

Prof. Scherer

9 / 16

Definitions of B. and P. from the OED:

Bourgeois 1. orig. A (French) citizen or freeman of a city or burgh, as
distinguished from a peasant on the one hand, and a gentleman on the
other; now often taken as the type of the mercantile or shopkeeping
middle class of any country. Also fem. bourgeoise, a Frenchwoman of
the middle class. [Latin Burgensis, German burg; compare borough]

Proletariate 2. Roman Hist. The lowest class of Roman citizens,
owning little or no property and with restricted rights, and regarded as
contributing nothing to the State except children. Also (occas.) in
extended use with reference to other ancient states. [Latin Proletarius]

Govt 101

Prof. Scherer

10 / 16

Definitions in Engels’s footnote:

By Bourgeoisie is meant the class of modern Capitalists, owners of the
means of social production and employers of wage-labour.

By Proletariate, the class of modern wage-labourers who, having no
means of production of their own, are reduced to selling their
labour-power in order to live.

Govt 101

Prof. Scherer

11 / 16

Necessary Conclusion to this story

“The essential conditions for the existence and for the sway of the
bourgeois class is the formation and augmentation of capital; the
condition for capital is wage-labour. Wage-labour rests exclusively on
competition between the labourers. The advance of industry, whose
involuntary promoter is the bourgeoisie, replaces the isolation of the
labourers, due to competition, by the revolutionary combination, due to
association. The development of Modern Industry, therefore, cuts from
under its feet the very foundation on which the bourgeoisie produces
and appropriates products. What the bourgeoisie therefore produces,
above all, are its own grave-diggers. Its fall and the victory of the
proletariat are equally inevitable.”

Govt 101

Prof. Scherer

12 / 16

Questions for discussion:
1

2

3

4

The bourgeoisie the proletariat are both economic and political
classes. What is a class? What are the Bourgeoisie and the Proletariat? Where do they come from and what do they do? What
obstacles do they face / what obstacles have they overcome? How
are they related and how diﬀerent?
“The bourgeoisie cannot exist without constantly revolutionising the
instruments of production, and thereby the relations of production,
and with them the whole relations of society." (476) What did this
mean in 1848?
Does the class analysis (question 1) still work today? If so how, if
not how should we think about class today?
Are the relations of production being revolutionized today? If so
how, if not what are the key relations of production?
Govt 101

Prof. Scherer

13 / 16

More questions for discussion:

1

2

3

“The ruling ideas of each age have ever been the ideas of its ruling
class." What did Marx think the ruling ideas of the 19th century
were? Are there ruling ideas today?—if so, What are they?
Marx says, "one fact is common to all past ages [and the present],
the exploitation of one part of society by the other" (488). Do you
agree? Is this a problem that "democracy" can and should adddress?
How, why, why not?
Marx and Engels call for the abolition of private property. Does
that call make sense today? Why or why not or what makes more
sense?

Govt 101

Prof. Scherer

14 / 16

Another way of putting this, two concepts:

I. Class antagonism
compare to Madison on faction
II. Private property
compare to Locke, Rousseau, Madison, Cronon, etc.

Govt 101

Prof. Scherer

15 / 16

Madison and Marx
. . . and divide them into diﬀerent classes, actuated by diﬀerent
sentiments and views. The regulation of these various and interfering
interests forms the principal task of modern legislation, and involves
the spirit of party and faction in the necessary and ordinary operations
of the government.
—Madison

The Communists disdain to conceal their views and aims. They openly
declare that their ends can be attained only by the forcible overthrow of
all existing social conditions. Let the ruling classes tremble at a
Communistic revolution. The proletarians have nothing to lose but
their chains. They have a world to win.
WORKING MEN OF ALL COUNTRIES, UNITE!
—Marx and Engels
Govt 101

Prof. Scherer

16 / 16

