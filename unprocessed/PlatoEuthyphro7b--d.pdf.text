8

e

7

b

c

PLATO

Socrates: Bear in mind then that I did not bid you tell me one or
two of the many pious actions but that form itself that makes all pious
actions pious, for you agreed that all impious actions are impious and
all pious actions pious through one form, or don’t you remember?
Euthyphro: I do.
Socrates: Tell me then what this form itself is, so that I may look
upon it and, using it as a model, say that any action of yours or another’s
that is of that kind is pious, and if it is not that it is not.
Euthyphro: If that is how you want it, Socrates, that is how I will
tell you.
Socrates: That is what I want.
Euthyphro: Well then, what is dear to the gods is pious, what is
not is impious.
Socrates: Splendid, Euthyphro! You have now answered in the way
I wanted. Whether your answer is true I do not know yet, but you will
obviously show me that what you say is true.
Euthyphro: Certainly.
Socrates: Come then, let us examine what we mean. An action or
a man dear to the gods is pious, but an action or a man hated by the
gods is impious. They are not the same, but quite opposite, the pious
and the impious. Is that not so?
Euthyphro: It is indeed.
Socrates: And that seems to be a good statement?
Euthyphro: I think so, Socrates.
Socrates: We have also stated that the gods are in a state of discord,
that they are at odds with each other, Euthyphro, and that they are at
enmity with each other. Has that, too, been said?
Euthyphro: It has.
Socrates: What are the subjects of difference that cause hatred and
anger? Let us look at it this way. If you and I were to differ about
numbers as to which is the greater, would this difference make us
enemies and angry with each other, or would we proceed to count and
soon resolve our difference about this?
Euthyphro: We would certainly do so.
Socrates: Again, if we differed about the larger and the smaller,
we would turn to measurement and soon cease to differ.
Euthyphro: That is so.

EUTHYPHRO

9

Socrates: And about the heavier and the lighter, we would resort
to weighing and be reconciled.
Euthyphro: Of course.
Socrates: What subject of difference would make us angry and
hostile to each other if we were unable to come to a decision? Perhaps
you do not have an answer ready, but examine as I tell you whether
these subjects are the just and the unjust, the beautiful and the ugly,
the good and the bad. Are these not the subjects of difference about
which, when we are unable to come to a satisfactory decision, you and
I and other men become hostile to each other whenever we do?
Euthyphro: That is the difference, Socrates, about those subjects.

d

Socrates: What about the gods, Euthyphro? If indeed they have
differences, will it not be about these same subjects?
Euthyphro: It certainly must be so.
Socrates: Then according to your argument, my good Euthyphro,
different gods consider different things to be just, beautiful, ugly, good,
and bad, for they would not be at odds with one another unless they
differed about these subjects, would they?
Euthyphro: You are right.
Socrates: And they like what each of them considers beautiful,
good, and just, and hate the opposites of these?
Euthyphro: Certainly.
Socrates: But you say that the same things are considered just by
some gods and unjust by others, and as they dispute about these things
they are at odds and at war with each other. Is that not so?
Euthyphro: It is.
Socrates: The same things then are loved by the gods and hated
by the gods, and would be both god-loved and god-hated.
Euthyphro: It seems likely.
Socrates: And the same things would be both pious and impious,
according to this argument?
Euthyphro: I’m afraid so.
Socrates: So you did not answer my question, you surprising man.
I did not ask you what same thing is both pious and impious, and it
appears that what is loved by the gods is also hated by them. So it is
in no way surprising if your present action, namely punishing your

e

8

b

