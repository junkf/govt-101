GOVT 101:
Frederick Douglass:
"The Meaning of the Fourth of July for the Negro"

Govt 101

Douglass

1 / 15

The Meaning of the Fourth of July for the Negro
What, to the American slave, is your 4th of July? I answer; a day that
reveals to him, more than all other days in the year, the gross injustice
and cruelty to which he is the constant victim. —Douglass, page 9.

Fellow-citizens, pardon me, allow me to ask, why am I called upon to
speak here to-day? What have I, or those I represent, to do with your
national independence? Are the great principles of political freedom
and of natural justice, embodied in that Declaration of Independence,
extended to us? and am I, therefore, called upon to bring our humble
offering to the national altar, and to confess the benefits and express
devout gratitude for the blessings resulting from your independence to
us? —Douglass, page 6.

Govt 101

Douglass

2 / 15

More sharply still

The blessings in which you, this day, rejoice, are not enjoyed in
common.-The rich inheritance of justice, liberty, prosperity and
independence, bequeathed by your fathers, is shared by you, not by me.
The sunlight that brought light and healing to you, has brought stripes
and death to me. This Fourth July is yours, not mine. You may rejoice,
I must mourn. To drag a man in fetters into the grand illuminated
temple of liberty, and call upon him to join you in joyous anthems, were
inhuman mockery and sacrilegious irony. Do you mean, citizens, to
mock me, by asking me to speak to-day? —Douglass, page 7.

Govt 101

Douglass

3 / 15

My subject, then, fellow citizens, is American slavery.

Whether we turn to the declarations of the past, or to the professions of
the present, the conduct of the nation seems equally hideous and
revolting. America is false to the past, false to the present, and
solemnly binds herself to be false to the future. Standing with God and
the crushed and bleeding slave on this occasion, I will, in the name of
humanity which is outraged, in the name of liberty which is fettered, in
the name of the constitution and the Bible which are disregarded and
trampled upon, dare to call in question and to denounce, with all the
emphasis I can command, everything that serves to perpetuate
slavery-the great sin and shame of America! — page 7.

Govt 101

Douglass

4 / 15

The time for argument is past

But I fancy I hear some one of my audience say, "It is just in this
circumstance that you and your brother abolitionists fail to make a
favorable impression on the public mind. Would you argue more, and
denounce less; would you persuade more, and rebuke less; your cause
would be much more likely to succeed." But, I submit, where all is
plain there is nothing to be argued. . . The time for argument is past
—pages 8 and 9.

Govt 101

Douglass

5 / 15

scorching irony, . . . biting ridicule, blasting reproach,
withering sarcasm, and stern rebuke

At a time like this, scorching irony, not convincing argument, is needed.
O! had I the ability, and could reach the nation’s ear, I would, today,
pour out a fiery stream of biting ridicule, blasting reproach, withering
sarcasm, and stern rebuke. For it is not light that is needed, but fire; it
is not the gentle shower, but thunder. We need the storm, the
whirlwind, and the earthquake. The feeling of the nation must be
quickened; the conscience of the nation must be roused; the propriety of
the nation must be startled; the hypocrisy of the nation must be
exposed; and its crimes against God and man must be proclaimed and
denounced. —page 9.

Govt 101

Douglass

6 / 15

Questions
1

Douglass says that Americans "cling" to slavery "as if it were the
sheet anchor of all your hopes" although it is really "a horrible reptile
coiled up in your nation’s bosom," (16). Why do Americans cling
to slavery according to Douglass? In general, why do people cling
to things that are bad (for them)?

2

A closely related question, if slavery is obviously wrong, why hadn’t
it been done away with? Consider what Douglass has to say about
"the timid and the prudent" on page 3. . . Does this apply today?

3

What was great about the founding generation (see e.g. 4–5)?
What’s wrong with the current generation (of Douglass’s day; also,
does it apply to ours)? How are those admirable and less than admirable qualities related?

4

If Douglass is not arguing that slavery is wrong (he says we already
know that) what is he doing in this speech?
Govt 101

Douglass

7 / 15

Corruption of republican principles

Americans! your republican politics, not less than your republican
religion, are flagrantly inconsistent. You boast of your love of liberty,
your superior civilization, and your pure Christianity, while the whole
political power of the nation (as embodied in the two great political
parties) is solemnly pledged to support and perpetuate the enslavement
of three millions of your countrymen. You hurl your anathemas at the
crowned headed tyrants of Russia and Austria and pride yourselves on
your Democratic institutions, while you yourselves consent to be the
mere tools and body-guards of the tyrants of Virginia and Carolina.
—page 15.

Govt 101

Douglass

8 / 15

Corruption: The slave trade

Fellow-citizens, this murderous traffic is, to-day, in active operation in
this boasted republic. In the solitude of my spirit I see clouds of dust
raised on the highways of the South; I see the bleeding footsteps; I hear
the doleful wail of fettered humanity on the way to the slave-markets,
where the victims are to be sold like horses, sheep, and swine, knocked
off to the highest bidder. There I see the tenderest ties ruthlessly
broken, to gratify the lust, caprice and rapacity of the buyers and
sellers of men. My soul sickens at the sight. —page 11.

Govt 101

Douglass

9 / 15

Corruption: The Fugitive Slave Law, Courts, and the
Rule of Law

The minister of American justice is bound by the law to hear but one
side; and that side is the side of the oppressor. Let this damning fact be
perpetually told. Let it be thundered around the world that in tyrantkilling, king-hating, people-loving, democratic, Christian America the
seats of justice are filled with judges who hold their offices under an
open and palpable bribe, and are bound, in deciding the case of a man’s
liberty, to hear only his accusers! —page 12.

Govt 101

Douglass

10 / 15

Churches and. . . Biblical Prophecy

In the language of Isaiah, the American church might be well
addressed, "Bring no more vain oblations; incense is an abomination
unto me: the new moons and Sabbaths, the calling of assemblies, I
cannot away with; it is iniquity, even the solemn meeting. Your new
moons, and your appointed feasts my soul hateth. They are a trouble
to me; I am weary to bear them; and when ye spread forth your hands I
will hide mine eyes from you. Yea’ when ye make many prayers, I will
not hear. Your hands are full of blood; cease to do evil, learn to do
well; seek judgment; relieve the oppressed; judge for the fatherless;
plead for the widow." —page 13.

Govt 101

Douglass

11 / 15

The Jeremiad

1

Praise the past: "your fathers, the fathers of this republic, did, most
deliberately, under the inspiration of a glorious patriotism, and with
a sublime faith in the great principles of justice and freedom, lay
deep, the corner-stone of the national super- structure, which has
risen and still rises in grandeur around you." (5)

2

Criticize the present: "My business, if I have any here to-day, is
with the present. The accepted time with God and His cause is the
ever-living now." (6)

3

To create a new future: "Allow me to say, in conclusion, notwithstanding the dark picture I have this day presented, of the state of
the nation, I do not despair of this country. There are forces in
operation which must inevitably work the downfall of slavery." (17)

Govt 101

Douglass

12 / 15

Declaration of Independence
Pride and patriotism, not less than gratitude, prompt you to celebrate
and to hold it in perpetual remembrance. I have said that the
Declaration of Independence is the ringbolt to the chain of your
nation’s destiny; so, indeed, I regard it. The principles contained in
that instrument are saving principles. Stand by those principles, be
true to them on all occasions, in all places, against all foes, and at
whatever cost. (4)

"WE hold these truths to be self-evident, that all men are created
equal, that they are endowed by their Creator with certain unalienable
Rights, that among these are Life, Liberty and the pursuit of
Happiness.–That to secure these rights, Governments are instituted
among Men, deriving their just powers from the consent of the
governed. . . "
Govt 101

Douglass

13 / 15

Constitution

"the Constitution is a glorious liberty document. Read its preamble,
consider its purposes. Is slavery among them? Is it at the gate way? or
is it in the temple? it is neither." (16)

“We the People of the United States, in Order to form a more perfect
Union, establish Justice, insure domestic Tranquility, provide for the
common defence, promote the general Welfare, and secure the Blessings
of Liberty to ourselves and our Posterity, do ordain and establish this
Constitution for the United States of America.”

Govt 101

Douglass

14 / 15

A Global vision (including commerce. . . )
While drawing encouragement from "the Declaration of Independence," the
great principles it contains, and the genius of American Institutions, my spirit
is also cheered by the obvious tendencies of the age. Nations do not now
stand in the same relation to each other that they did ages ago. No nation
can now shut itself up from the surrounding world and trot round in the same
old path of its fathers without interference. The time was when such could be
done. Long established customs of hurtful character could formerly fence
themselves in, and do their evil work with social impunity. Knowledge was
then confined and enjoyed by the privileged few, and the multitude walked on
in mental darkness. But a change has now come over the affairs of mankind.
Walled cities and empires have become unfashionable. The arm of commerce
has borne away the gates of the strong city. Intelligence is penetrating the
darkest corners of the globe. It makes its pathway over and under the sea, as
well as on the earth. Wind, steam, and lightning are its chartered agents.
Oceans no longer divide, but link nations together. From Boston to London is
now a holiday excursion. Space is comparatively annihilated.-Thoughts
expressed on one side of the Atlantic are distinctly heard on the other.
Govt 101

Douglass

15 / 15

